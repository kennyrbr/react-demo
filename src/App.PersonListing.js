import "./App.css";
import { useState, useEffect } from "react";
import Person from "./components/Person.js";
import { PersonListing } from "./components/PersonListing";
import { getData } from "./data";

function App() {
  const [mypeople, setMypeople] = useState([]);
  const [error, setError] = useState(null);
  useEffect(() => {
    getData()
      .then((people) => setMypeople(people))
      .catch((err) => setError(err));
  }, []);

  const editPerson = (id, name, nickname) => {
    console.log(`@editPerson(${id}, ${name}, ${nickname})`);

    const newpeople = mypeople.map((person) =>
      person.id === id ? { ...person, name, nickname } : person
    );

    setMypeople(newpeople);
  };

  return error ? (
    <p>{error.message}</p>
  ) : !mypeople.length ? (
    <p>no data</p>
  ) : (
    <div className="App">
      <div>
        {mypeople.map((person) => {
          return (
            <Person
              key={person.id}
              id={person.id}
              name={person.name}
              nickname={person.nickname}
              editPerson={editPerson}
            />
          );
        })}
      </div>

      <PersonListing people={mypeople} />
    </div>
  );
}

export default App;

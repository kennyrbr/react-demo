const people = [
  {
    id: 1,
    name: "Kenn",
    nickname: "kennyb",
  },
  {
    id: 2,
    name: "Leanne",
    nickname: "Mesh",
  },
  {
    id: 3,
    name: "Jett",
    nickname: "JT",
  },
  {
    id: 4,
    name: "Finn",
  },
];

export const getData = async () => people;
// export const getData = async () => {
//   throw new Error("whoops");
// };

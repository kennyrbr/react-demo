import React, { useEffect, useState, useContext } from 'react';
import { UserContext, ChannelContext } from '../App';
import './styles.css';

export const Post = ({ post, globalHideBody }) => {
  const [hideBody, setHideBody] = useState(globalHideBody);
  useEffect(() => {
    setHideBody(globalHideBody);
  }, [globalHideBody]);
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);

  return (
    <div>
      <h2 onClick={() => setHideBody(!hideBody)}>{post.title}</h2>
      <div className={hideBody ? 'hideme' : null}>
        <p>{post.body}</p>
        <p>{`${user.firstName} ${user.lastName} - ${channel.book}`}</p>
      </div>
    </div>
  );
};

import React, { useEffect, useState } from 'react';

export const SimpleFormValidation = () => {
  const initialState = {
    firstName: '',
    lastName: ''
  };
  const [displayFirstName, setDisplayFirstName] = useState(false);
  const [formState, setFormState] = useState(initialState);
  const [validForm, setValidForm] = useState(false);
  const submitForm = (e) => {
    e.preventDefault();
    console.log('formState');
    console.log(formState);
    alert('submitting...');
  };
  useEffect(() => {
    console.log('@useEffect with formState');
    console.log(formState);

    (() => {
      console.log('@validateForm: formState', formState);
      const valid = formState.firstName !== '' && formState.lastName !== '';
      setValidForm(valid);
    })();
  }, [formState]);
  const handleFirstNameChange = (e) => {
    console.log(`@handleFirstNameChange`, e.target.value);
    setFormState((prevFormState) => ({ ...prevFormState, firstName: e.target.value }));
  };
  const handleLastNameChange = (e) => {
    console.log(`@handleLastNameChange`, e.target.value);
    setFormState((prevFormState) => ({ ...prevFormState, lastName: e.target.value }));
  };

  return (
    <>
      <h1>SimpleFormValidation</h1>
      <form onSubmit={submitForm}>
        <div>
          <label htmlFor="first_name" onMouseOver={() => setDisplayFirstName(true)}>
            First Name:{' '}
          </label>
          <input
            name="first_name"
            type="text"
            placeholder="Enter your first name"
            style={{ display: !displayFirstName ? 'none' : null }}
            value={formState.firstName}
            onChange={handleFirstNameChange}
          ></input>
        </div>

        <div>
          <label htmlFor="last_name">Last Name: </label>
          <input
            name="last_name"
            type="text"
            placeholder="Enter your last name"
            value={formState.lastName}
            onChange={handleLastNameChange}
          ></input>
        </div>

        <div>
          <button type="submit" disabled={!validForm}>
            Submit
          </button>
        </div>
      </form>
    </>
  );
};

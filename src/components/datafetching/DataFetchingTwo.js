import React, { useEffect, useReducer } from 'react';
import axios from 'axios';

/*
 * Kenn's attempt before watching the tutorial
const initialLoadingState = 'Loading...';
const loadingReducer = (state, action) => {
  switch (action) {
    case 'loaded':
    case 'error':
    default:
      return '';
  }
};
const initialPostState = {};
const postReducer = (state, action) => {
  switch (action.type) {
    case 'loaded':
      return action.value;
    case 'error':
    default:
      return {};
  }
};
const initialErrorState = '';
const errorReducer = (state, action) => {
  switch (action.type) {
    case 'loaded':
      return '';
    case 'error':
    default:
      return 'Something went wrong: ' + action.value;
  }
};
 * end: Kenn's attempt before watching the tutorial
 */

const initialState = {
  loading: true,
  error: '',
  post: {}
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'FETCH_SUCCESS':
      return {
        loading: false,
        post: action.payload,
        error: ''
      };
    case 'FETCH_ERROR':
      return {
        loading: false,
        post: {},
        error: 'Something went wrong'
      };
    default:
      return state;
  }
};

export const DataFetchingTwo = () => {
  // const [post, dispatchPost] = useReducer(postReducer, initialPostState);
  // const [loading, dispatchLoading] = useReducer(loadingReducer, initialLoadingState);
  // const [error, dispatchError] = useReducer(errorReducer, initialErrorState);
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    axios
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .then((response) => {
        // dispatchLoading('loaded');
        // dispatchPost({ type: 'loaded', value: response.data });
        // dispatchError('loaded');
        dispatch({ type: 'FETCH_SUCCESS', payload: response.data });
      })
      .catch((error) => {
        // dispatchLoading('error');
        // dispatchPost({ type: 'error' });
        // dispatchError({ type: 'error', value: error });
        dispatch({ type: 'FETCH_ERROR' });
      });
  }, []);
  return (
    <div>
      <div>DataFetchingTwo</div>
      <div>
        {state.loading ? 'Loading' : state.post.title}
        {state.error ? state.error : null}
      </div>
    </div>
  );
};

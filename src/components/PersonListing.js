import React from "react";

export const PersonListing = (props) => {
  return (
    <div>
      <h1>Person Listing</h1>
      {!props.people ? (
        <p>no people to list</p>
      ) : (
        props.people.map((person) => (
          <p key={person.id}>
            {person.name} {person.nickname}
          </p>
        ))
      )}
    </div>
  );
};

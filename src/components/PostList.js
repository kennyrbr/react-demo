import React, { useEffect, useState } from 'react';
import { UserContext, ChannelContext } from '../App';
import { Post } from './Post';
import './styles.css';

export const PostList = () => {
  const [filter, setFilter] = useState('');
  const [ordering, setOrdering] = useState('asc');
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    console.log();
    console.log('fetching...');
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((posts) => {
        setPosts(posts);
      });
    return () => {
      console.log('unmounting PostList');
    };
  }, []);
  const [hidePostBodies, setHidePostBodies] = useState(true);

  return (
    <div>
      <UserContext.Consumer>
        {(user) => {
          return (
            <ChannelContext.Consumer>
              {(channel) => {
                return (
                  <div>
                    User context value: {user.firstName} {user.lastName}.
                    Channel context value: {channel.book} {channel.chapter}:
                    {channel.verses}
                  </div>
                );
              }}
            </ChannelContext.Consumer>
          );
        }}
      </UserContext.Consumer>
      <h1 onClick={() => setHidePostBodies(!hidePostBodies)}>PostList</h1>
      <label>filter: </label>
      <input
        type="text"
        value={filter}
        onChange={(e) => setFilter(e.target.value)}
      />
      <label>order:</label>
      <select value={ordering} onChange={(e) => setOrdering(e.target.value)}>
        <option value="asc">ascending</option>
        <option value="desc">descending</option>
      </select>
      {posts
        .filter((post) => filter === '' || post.title.includes(filter))
        .sort((a, b) => {
          if (ordering === 'asc') {
            return a.title < b.title ? -1 : 1;
          } else {
            return b.title < a.title ? -1 : 1;
          }
        })
        .map((post) => {
          return (
            <Post key={post.id} post={post} globalHideBody={hidePostBodies} />
          );
        })}
    </div>
  );
};

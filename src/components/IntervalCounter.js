import React, { useEffect, useState } from 'react';

export const IntervalCounter = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const tick = () => {
      setCount((prevCount) => prevCount + 1);
    };

    const interval = setInterval(tick, 1000);
    console.log('setting interval: ', interval);

    return () => {
      console.log('unmounting...');
      console.log('clearing interval: ', interval);
      clearInterval(interval);
    };
  }, []);

  return <h1>{count}</h1>;
};

import React from 'react';

const ParentComponent = () => {
  return <div>ParentComponent</div>;
};

export default React.memo(ParentComponent);

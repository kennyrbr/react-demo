import React, { useState } from 'react';
import './styles.css';

const Person = ({ id, name, nickname = '', editPerson }) => {
  const [myname, setMyname] = useState(name);
  const [mynickname, setMynickname] = useState(nickname);
  const [nameChanged, setNameChanged] = useState(null);
  const [nicknameChanged, setNicknameChanged] = useState(null);
  const message = !!name
    ? `Hello ${myname}! ...or should I say ${mynickname}!`
    : `Hello ${myname}!`;
  return (
    <>
      <h2>Hello {message}</h2>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          setNameChanged(false);
          setNicknameChanged(false);
          editPerson(id, myname, mynickname);
        }}
      >
        <div>
          <label className={nameChanged ? 'changed' : null}>name</label>
          <input
            type="text"
            value={myname}
            onChange={(event) => {
              setNameChanged(true);
              setMyname(event.target.value);
            }}
          />

          <label className={nicknameChanged ? 'changed' : null}>nickname</label>
          <input
            type="text"
            value={mynickname}
            onChange={(event) => {
              setNicknameChanged(true);
              setMynickname(event.target.value);
            }}
          />
        </div>
        <button type="submit">Change me</button>
      </form>
    </>
  );
};

export default Person;

import React, { useContext } from 'react';
import { CountContext } from '../App';

export const GlobalReducerCounterA = () => {
  const { countState, countDispatch } = useContext(CountContext);

  return (
    <div>
      <h1>GlobalReducerCounterA</h1>
      <div>
        <div>Count: {countState}</div>
        <button onClick={() => countDispatch('increment')}>Increment</button>
        <button onClick={() => countDispatch('decrement')}>Decrement</button>
        <button onClick={() => countDispatch('reset')}>Reset</button>
      </div>
    </div>
  );
};

export default GlobalReducerCounterA;

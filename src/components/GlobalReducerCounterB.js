import React, { useContext } from 'react';
import { CountContext } from '../App';

export const GlobalReducerCounterB = () => {
  const { countState, countDispatch } = useContext(CountContext);

  return (
    <div>
      <h1>GlobalReducerCounterB</h1>
      <div>
        <div>Count: {countState}</div>
        <button onClick={() => countDispatch('increment')}>Increment</button>
        <button onClick={() => countDispatch('decrement')}>Decrement</button>
        <button onClick={() => countDispatch('reset')}>Reset</button>
      </div>
    </div>
  );
};

import './App.css';
import React, { useState, useReducer } from 'react';
import { PostList } from './components/PostList';
import { PersonList } from './components/PersonList';
import { IntervalCounter } from './components/IntervalCounter';
import { ReducerCounter } from './components/ReducerCounter';
import { ReducerComplexCounter } from './components/ReducerComplexCounter';
import { GlobalReducerCounterA } from './components/GlobalReducerCounterA';
import { GlobalReducerCounterB } from './components/GlobalReducerCounterB';
import { DataFetchingOne } from './components/datafetching/DataFetchingOne';
import { DataFetchingTwo } from './components/datafetching/DataFetchingTwo';
import DocTitleTwo from './components/customhooks/DocTitleTwo';
import { SimpleFormValidation } from './components/SimpleFormValidation';

export const UserContext = React.createContext();
export const ChannelContext = React.createContext();
const user = { id: 1, firstName: 'Kenn', lastName: 'Baker' };
const channel = { id: 1, book: '1 John', chapter: '5', verses: '13-15' };

//
// React Hooks Tutorial 22 - useReducer + useContext
// https://www.youtube.com/watch?v=BCD2irXaVoE&list=PLC3y8-rFHvwgg3vaYJgHGnModB54rxOk3&index=65
export const CountContext = React.createContext();
const initialState = 0;
export const reducer = (state, action) => {
  switch (action) {
    case 'increment':
      return state + 1;
    case 'decrement':
      return state - 1;
    case 'reset':
      return initialState;
    default:
      return state;
  }
};

function App() {
  const [display, setDisplay] = useState('');
  const [count, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="App">
      <UserContext.Provider value={user}>
        <ChannelContext.Provider value={channel}>
          <CountContext.Provider value={{ countState: count, countDispatch: dispatch }}>
            <div>
              <div>Count: {count}</div>
            </div>
            <select value={display} onChange={(e) => setDisplay(e.target.value)}>
              <option value="">--Please select a component to display --</option>
              <option value="datafetchingone">DataFetchingOne</option>
              <option value="datafetchingtwo">DataFetchingTwo</option>
              <option value="doctitletwo">DocTitleTwo</option>
              <option value="grcountera">GlobalReducerCounterA</option>
              <option value="grcounterb">GlobalReducerCounterB</option>
              <option value="intervalcounter">IntervalCounter</option>
              <option value="personlist">PersonList</option>
              <option value="postlist">PostList</option>
              <option value="reducercomplexcounter">ReducerComplexCounter</option>
              <option value="reducercounter">ReducerCounter</option>
              <option value="simpleformvalidation">SimpleFormValidation</option>
            </select>

            {display === 'datafetchingone' && <DataFetchingOne />}
            {display === 'datafetchingtwo' && <DataFetchingTwo />}
            {display === 'doctitletwo' && <DocTitleTwo />}
            {display === 'grcountera' && <GlobalReducerCounterA />}
            {display === 'grcounterb' && <GlobalReducerCounterB />}
            {display === 'intervalcounter' && <IntervalCounter />}
            {display === 'personlist' && <PersonList />}
            {display === 'postlist' && <PostList />}
            {display === 'reducercomplexcounter' && <ReducerComplexCounter />}
            {display === 'reducercounter' && <ReducerCounter />}
            {display === 'simpleformvalidation' && <SimpleFormValidation />}
          </CountContext.Provider>
        </ChannelContext.Provider>
      </UserContext.Provider>
    </div>
  );
}

export default App;
